#Universidac Central del Ecuador
#Facultad de Ingeniería y Ciencias Aplicadas
#Ingeniería en Computación Gráfica
#Geometría Computacional
#Integrantes: Gómez Valeria, Ipiales Sandra, Guaminga Alejandra
#Enero 2021

#Averiguar si un punto es interno o externo a un triángulo
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import random


global x

def PuntosAlea(cant):
    return [round(random.uniform(0,10),2) for _ in range(cant)]
#Ingreso de puntos
op= input('1.Ingresar los numeros o 2.Generarlos aleatoriamente: ')
print(op)
if op == "1":
    print('Ingrese los puntos ABC del triángulo: ')
    x1 = float(input('Coordenada x de A: '))
    y1 = float(input('Coordenada y de A: '))
    x2 = float(input('Coordenada x de B: '))
    y2 = float(input('Coordenada y de B: '))
    x3 = float(input('Coordenada x de C: '))
    y3 = float(input('Coordenada y de C: '))
    print('Ingrese el punto a evaluar: ')
    px = float(input('Coordenada x: '))
    py = float(input('Coordenada y: '))
else:
    P=PuntosAlea(8)
    print("Los puntos son: ", P)
    x1 = P[0]
    y1 = P[1]
    x2 = P[2]
    y2 = P[3]
    x3 = P[4]
    y3 = P[5]
    px = P[6]
    py = P[7]


#calcular área
def Area(x1, y1, x2, y2, x3, y3):
    return round(abs((x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2)))/2, 20)

#calcular áreas parciales
if ((x2-x1)*(y3-y1) - (y2-y1)*(x3-x1) ==0):
    print('Los puntos no forman un triángulo')
else:
    print('Los puntos forman un triángulo')
    area_triangulo=Area(x1,y1,x2,y2,x3,y3)
    area1=Area(px, py, x2, y2, x3, y3)
    area2=Area(x1, y1, px, py, x3, y3)
    area3=Area(x1, y1, x2, y2, px, py)
    if area1 == 0 or area2 ==0 or area3==0:
        print('El punto es colineal')
    #muestra si el punto dado está dentro o fuera del triángulo
    if(area_triangulo>=(area1+area2+area3)):
        print('El punto dado es interior al triángulo')
        x = 'green'
    else:
        print('El punto dado es exterior al triángulo')
        x = 'red'

def grafico():
    plt.scatter(px, py, s=20)
    p = Polygon([[x1,y1], [x2,y2], [x3,y3]],fill=False,edgecolor=x)
    ax = plt.gca()
    ax.add_patch(p)
    ax.set_xlim(0, 10)
    ax.set_ylim(0, 10)
    plt.show()


grafico()